// Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// DeveloperSwitch implementation

#include <sys/types.h>
#include "developer_switch.h"

namespace cros_boot_mode {

DeveloperSwitch::DeveloperSwitch() { }
DeveloperSwitch::~DeveloperSwitch() { }

}  // namespace cros_boot_mode
